---
layout: post
title: Apache 모듈 4.3 이후 Multipart Upload 방법
date: 2013-12-12 21:30
---
시코 모바일용 서버에 사용하던 Apache 라이브러리를 최신으로 바꾸었다.

{:.uk-table}
| 라이브러리 | 기존 버전 | 현재 버전 |
| :-----: | :-----: | :-----: |
|  httpclient  |   4.2.5   |  4.3.1  |
|  httpcore   |   4.2.4   |  4.3    |
|  httpmime  |   4.2.5   |  4.3.1  |

사실 멀쩡히 돌아가는 서버는 웬만하면 안건드리는게 좋은데, 개인적으로 뭐가 됐든 그냥 최신버전을 쓰는게 좋아서 업데이트 해보기로 했다. 라이브러리를 교체하고 나니 경고가 뜨길래 뭔가하고 봤더니 멀티파트 업로드 방식이 4.3 이후로 기존 방법은 Deprecated 되었던 것이었다.

기존의 방법으로 문자열을 Multipart Upload 하려면 아래와 같이 MultipartEntity를 선언하고 addPart 메서드를 통해 StringBody 등을 넣어준 다음 HttpPost 객체에 넣고 Request를 보내야 했었다.

{% highlight java %}
//MultipartEntity 객체 선언
//boundaryString은 Multipart 업로드시 사용할 임의의 String값
//charset은 자바에서 기본적으로 사용할 수 있는 Charset 객체
MultipartEntity reqEntity;
reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, boundaryString, charset);

//tempString을 StringBody 객체에 넣고 MultipartEntity에 넣음
reqEntity.addPart("part", new StringBody(tempString));

//HttpPost에 삽입
httppost.setEntity(reqEntity);
{% endhighlight %}

그런데 이 방법에서 MultipartEntity와 StringBody 등이 Deprecated 되었고 새로 MultipartEntityBuilder가 추가되었다. 이는 다음과 같이 사용한다.

{% highlight java %}
//MultipartEntityBuilder를 다음과 같이 선언
MultipartEntityBuilder meb = MultipartEntityBuilder.create();

//Builder 설정하기.
//선언할때 넣는게 아니라 선언 후 메소드로 설정한다.
meb.setBoundary(boundaryString);
meb.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
meb.setCharset(charset);

//문자열을 보내려면 addPart와 StringBody가 아닌 addTextBody를 사용한다.
meb.addTextBody("part", tempString);

//HttpEntity를 빌드하고 HttpPost 객체에 삽입한다.
HttpEntity entity = meb.build();
httppost.setEntity(entity);
{% endhighlight %}

StringBody같은 객체를 선언할 일이 이전보다 줄어서 예전보다 좀더 코드가 깔끔해진 느낌이다.
