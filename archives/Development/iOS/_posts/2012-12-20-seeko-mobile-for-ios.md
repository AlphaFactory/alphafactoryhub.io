---
layout: post
title: Seeko Mobile for iOS
date: 2012-12-20 16:37
image: http://i.imgur.com/5jchV1h.jpg
thumbnail: http://i.imgur.com/5jchV1hb.jpg
---
제작중인 Seeko Mobile for iOS. HTML5 기반 웹앱이다. 얼마전부터 HTML5를 공부하기 시작한건 전부 이것때문이라고 보면 된다. 이거 만들다가 보니, 덕분에 어지간한 모바일 웹사이트는 만들수 있게 된 것 같다. 이거 다 만들고 나면 조만간 내 텀블러 모바일페이지나 한번 건드려봐야겠다.

서버사이드에서 파싱모듈 다 처리하게 만들어 놓으니까 클라이언트단에선 뭘 할게 없어서 참 좋다고 생각한다. 실제로 이거 개발도 굉장히 빠르게 진행되고 있고.