---
layout: post
title: Seeko Mobile V2 Prototype
date: 2012-12-14 16:12
image: http://i.imgur.com/ZBVMqCN.jpg
thumbnail: http://i.imgur.com/ZBVMqCNb.jpg
---
개발 중인 시코 모바일 V2. ICS 기반으로 만들고 있다. 사실 이전까지의 시코 모바일은 이클레어를 기준으로 만들던 놈이다보니 아무래도 좀 힘든 면이 많았는데, 아예 아샌을 기반으로 제작을 시작하고 보니, 그래도 요즘 안드로이드 앱들의 UI/UX 트렌드를 따라갈 수 있게 된 것 같아 기분이 좋다.

사이드 네비게이션 UI는 다른 앱에 들어간 걸 사용만 해봤지 직접 구현하는건 이번이 처음인데, 생각보다는 쉬웠지만 앞으로 상당한 난관이 보일게 뻔해서 굉장히 긴장하면서 코딩하고 있다.
