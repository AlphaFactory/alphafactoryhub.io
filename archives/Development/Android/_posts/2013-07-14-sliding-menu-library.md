---
layout: post
title: 슬라이딩 메뉴(Sliding Menu) 라이브러리
date: 2013-07-14 01:41
---
화면을 왼쪽에서 오른쪽으로 드래그, 혹은 스와이프 하면 왼쪽에서 부드러운 애니메이션과 함께 짠 하고 나타나는 슬라이딩 메뉴는 요즘 앱들의 대세적인 UX/UI라고 보아도 과언이 아니다. 홈버튼을 제외하면 화면을 조작하기 위한 다른 하드웨어 버튼이 존재하지 않는 iPhone, iPad의 경우 이러한 슬라이딩 메뉴는 상당히 오래전부터 유행했었고, 구글의 기본 앱들이 최근들어 업데이트되면서 거의 모든 앱에 해당 기능이 나타났다.

문제는 이러한 슬라이딩 메뉴를 어떻게 구현하느냐이겠다. 커뮤니티를 좀 돌아다니다 보면, 안드로이드의 경우 Drawer라는 위젯이 예전부터 있었기 때문에 이를 이용하여 직접 만들어보라고 하거나, 혹은 꽤 오래된 소스코드를 추천해준다.

나의 경우는 이 라이브러리를 추천하고 싶다.

[https://github.com/jfeinstein10/SlidingMenu][url]

이 라이브러리 역시 따로 레이아웃을 손댈 필요가 없는 아주 편한 오픈소스 라이브러리로써, 아래와 같이 사용하면 된다.

{% highlight java %}
public class SlidingExample extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.attach);
        // set the content view
        setContentView(R.layout.content);
        // configure the SlidingMenu
        SlidingMenu menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
    }
}
{% endhighlight %}

보면 알겠지만, SlidineMenu 객체를 선언하고, 잡다한 몇가지 설정을 해준 뒤, attachToActivity 메서드를 사용하면 놀랍게도 위의 화면과 같이 슬라이딩 메뉴를 만들 수 있다.

TouchMode를 바꾸면 스와이프 제스쳐를 인식하는 범위를 설정 가능하고, attachToActivity 메소드를 사용할때 SLIDING_CONTENT가 아니라 SLIDING_WINDOW로 바꾸면 화면 전체가 이동하면서 메뉴가 나타나도록 만들 수도 있다. setMenu는 메뉴에 나타날 XML Layout을 지정해주는 것으로서, 얼마든지 커스텀을 할 수 있다.

XML Layout 내에서 사용하는 방법도 있지만, 개인적으로는 위와같이 자바코드 몇줄 치는게 훨씬 더 사용하기 편했다.

위의 이미지는 현재 개발중인 Seeko Mobile V2 어플리케이션으로, 아주 잘 작동하고 있다. 메뉴에 나타나는 그림자와 페이딩 애니메이션 등도 설정할 수 있기 때문에 적은 노력으로도 굉장히 좋은 효과를 볼 수 있다. 이 라이브러리의 신뢰도는 Wunderlist, Evernote Food와 같은 매우 우수한 앱들이 사용하고 있다는 점에서 이미 증명된 것 아닐까.

[url]: https://github.com/jfeinstein10/SlidingMenu