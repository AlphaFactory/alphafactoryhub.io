---
layout: post
title: 당겨서 새로고침(Pull-To-Refresh) 리스트뷰 라이브러리
date: 2013-07-17 08:24
---
당겨서 새로고침은 안드로이드가 제대로 흥하기 이전, iOS에서부터 인기를 끌기 시작했다. 크기가 작고, 따로 외부버튼이 없던 아이폰에서 당겨서 새로고침(Pull-To-Refresh)는 참 획기적인 UX중 하나였었다. 그런데, 운영체제의 차이 때문인지 이걸 안드로이드에서는 구현하기가 매우 어려웠던 모양이다.

일단 기본적으로 제공해주는 UI Component로는 만들기가 어려웠었다. Listview의 HeaderView에 붙여서 만들기도 하고, ListView의 첫번째 리스트아이템과 스크롤바 위치를 속여서 만들기도 하는 등 했지만, 아이폰만큼의 깔끔함은 보여주지 못했다. (사실 이건 그 시절 안드로이드 UI Thread Priority 문제도 있는 것 같긴 하지만)

당겨서 새로고침 하는 리스트뷰 라이브러리 혹은 소스코드만 한 대여섯개는 찾아서 써본거 같은데 이 라이브러리가 가장 깔끔함을 보여주고 있었다.

[https://github.com/chrisbanes/Android-PullToRefresh][url]

지난번 포스팅을 본 사람은 chrisbanes라는 단어가 조금 익숙할 텐데, 이 사람은 지난번에 소개했던 핀치투줌 라이브러리를 만들었던 사람이기도 하다.

이 라이브러리는 비단 일반적인 리스트뷰 뿐만 아니라 그리드뷰나 웹뷰 등에도 적용할 수 있어 개발자의 노고를 굉장히 줄여줄 수 있다. Apache Licensen 2.0이라 상용으로 쓰는데도 문제 없다.

나의 경우 예전에 외주받아 제작했던 Hot Money앱과 현제 제작중인 Seeko Mobile V2에 본 라이브러리를 사용하고 있다.

[url]: https://github.com/chrisbanes/Android-PullToRefresh