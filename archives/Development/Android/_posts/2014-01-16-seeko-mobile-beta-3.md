---
layout: post
title: Seeko Mobile V2 Beta 3 배포 시작
date: 2015-01-23 03:23
thumbnail: http://i.imgur.com/iZmCmh8b.png
tags: [android, beta-test, application, seekomobile]
---
###잡설

세번째 베타버전을 배포하기로 했다. 지난 베타버전 업로드 이후 1년만의 업데이트다.

1년 사이에 Android 5.0 Lolipop이 발표되고 새로운 UI인 Material Design이 발표되기도 하는 등 많은 일이 있어, Beta 2때와는 사뭇 다른 모습의 앱이 완성되었다. Ver 2.0만 거의 2년 넘게 만들어오고 있는 것 같은데 베이퍼웨어화 되기 전에 얼른 마켓에 업로드하고싶다.

기존버전과 가장 큰 차이점이라고 한다면 이제 내 중계서버가 아니라 시코 본서버로 접속하여 시코에서 제공해주는 API를 제대로 활용할 수 있게 되었다는 점이다. 단, 아직 기능이 많이 부족하여 어쩔수 없이 구현이 불가한 부분들이 많았지만, 그에 비해 얻은 것이 많으므로 차차 업데이트 해 나가며 수정하도록 하자.

---

###베타 테스터 분들께

본 베타 테스트는 마켓에 업로드만 안했다 뿐이지 완전히 공개된 상태로 진행하는 것으로, 이는 최대한 빨리 최대한 많은 피드백을 얻기 위함입니다.

![Seeko Mobile V2 Beta 3 Preview Screenshot][image1]

이번에 공개하는 APK 파일은 버전코드 200150123, 버전명 Ver 2.0.0 Beta 3로써, 기존과의 변경점은 다음과 같습니다.

1. 기존 Seeko Mobile 전용 중계서버 연동 삭제
2. Seeko 사이트에서 제공하는 모바일용 API 적용
3. Google Material Design 적용
4. 화면 좌측에 카테고리 목록으로 사용하기 위한 Navigation Drawer 도입
5. 카테고리 목록 상단으로 로그인/로그아웃 인터페이스 이동
6. 당겨서 새로고침(SwipeRefreshLayout) 적용
7. 댓글 달기 인터페이스를 팝업 다이얼로그 방식으로 변경
8. 태블릿 최적화(추후 UI를 좀더 변경할 예정)
9. 게시글 관련 메뉴(URL 복사, 웹페이지 보기 등)을 게시글 화면의 상단 버튼이 아니라 게시판 글목록에서 롱클릭시 팝업되도록 변경
10. 게시글 쓰기 인터페이스 대폭 개선

###참고사항 몇가지

1. 인앱결제 및 기부자 관련 부분을 비활성화 하여 본 Beta 3에 한하여 광고제거가 불가합니다.
2. 태블릿 UI가 아직 완벽하지 않습니다.

###베타테스트 참여 방법

베타버전은 구글이 제공하는 베타테스트 방법을 사용하므로, 아래의 순서에 맞게 베타테스트 신청을 해주신 후 기다리면 앱을 마켓에서 업데이트하는 형식으로 베타버전을 받아보실 수 있습니다.

우선 [Seeko Mobile 구글 플러스 커뮤니티][url1]에 가입합니다. 그리고 나서 커뮤니티 정보란에 있는 [베타테스트 승인 링크][url2]를 누르면, 베타테스트 신청화면이 나올것입니다. 신청하신 후 마켓에 정보가 반영되기를 기다리면, 약 1~2시간 후 베타테스트 버전을 마켓에서 받아보실 수 있습니다.

위 절차가 번거로우신 분들은 [드랍박스 링크][dropbox]를 통해 받아서 수동업데이트 하시면 되겠습니다.

[image1]: http://i.imgur.com/iZmCmh8.png

[url1]: https://plus.google.com/communities/108484091700158311280
[url2]: https://play.google.com/apps/testing/com.alphafactory.seekomobile
[dropbox]: https://www.dropbox.com/s/22abx8mml8y71bz/v2_beta_3_hotfix.apk?dl=0