---
layout: post
title: 핀치 투 줌(Pinch-To-Zoom) 라이브러리
date: 2013-07-13 14:04
---
이미지를 표기하는 앱에 있어서 가장 중요한 것 중 하나라고 한다면, 이미지 화질이나 캐시같은 문제도 있겠지만 개인적으로는 핀치투줌 기능을 넣는 것이라 본다. (시코 모바일 V1에서는 이 기능을 귀찮아서 안넣었다가 필요성을 절실하게 깨닫고 V2에는 집어넣었다.) 헌데 안드로이드 쪽은 이 기능을 구현하기 위해서 쓰는 방법이 널리 알려진 안드로이드 1.5시절 소스를 가져다 쓰는 것인데, 이럴 필요 없이 이걸 아주 간단한 방법으로 해결해 주는 라이브러리를 찾았다.

[https://github.com/chrisbanes/PhotoView][url]

특이한게 이미지뷰는 그대로 가져다 쓰면 되고, 건드려야하는 것은 이 라이브러리에서 제공하는 PhotoViewAttacher 클래스.

{% highlight java %}
ImageView mImageView;
PhotoViewAttacher mAttacher;

@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // Any implementation of ImageView can be used!
    mImageView = (ImageView) findViewById(R.id.iv_photo);

    // Set the Drawable displayed
    Drawable bitmap = getResources().getDrawable(R.drawable.wallpaper);
    mImageView.setImageDrawable(bitmap);

    // Attach a PhotoViewAttacher, which takes care of all of the zooming fu    nctionality.
    mAttacher = new PhotoViewAttacher(mImageView);
}

// If you later call mImageView.setImageDrawable/setImageBitmap/setImageResource/etc then you just need to call
attacher.update();
{% endhighlight %}

위의 방법대로 사용하면 된다. 굳이 소스 복사 붙여넣기 해서 할 필요 없고, 위와 같이 PhotoViewAttacher 클래스를 선언하고 update() 메소드를 한번 실행해주면 자동으로 알아서 핀치투줌 기능을 구현해준다. 핀치투줌 이외에도 더블클릭시 2단계의 확대를 지원하니 상당히 편리한 라이브러리라 볼 수 있겠다.

아직까지 이것보다 편리한 방법의 핀치투줌 라이브러리는 찾지 못한것 같다. 강력 추천하는 바이다.

[url]: https://github.com/chrisbanes/PhotoView