---
layout: post
title: 안드로이드 앱 빌드 시 aapt.exe 중단 문제 해결책
date: 2014-01-03 10:14
thumbnail: http://i.imgur.com/GcDGvhWb.png
---
시코 모바일 V2를 준비하면서 코딩에 여념이 없는데 앱 사용 통계를 보니 여전히 많은 수의 사용자가 안드로이드 4.0 아이스크림 샌드위치 미만의 OS를 사용하고 있는 것을 확인할 수 있었다. 그래서 `ActionBarSherlock`을 사용하기로 마음먹고 라이브러리를 다운로드 받아 라이브러리로 사용하겠다고 설정을 한 후 빌드를 해보니 `aapt.exe`가 중지되었다는 메시지와 함께 빌드가 되지 않는 것이었다. 에러코드는 `-1073741819`.

최근들어 이클립스가 뭔가 에러를 뱉어낸 적이 거의 없었던지라 솔직히 당황했다. 사실 라이브러리 추가하기 전에는 멀쩡했기 때문에 도대체 뭐가 문제인지 알수가 없었다. 뭔가 내가 잘못했나 싶어서 라이브러리에서 `ActionBarSherlock`을 제거하고 보니 또 빌드가 잘된다.

![기다려. 당황하지 마라. 이건 이클립스의 함정이다.][image1]

도대체 내가 뭘 잘못한 건지 알수가 없었기에 나는 `Android Studio`를 다운로드 받아서 사용해보기로 했다. 0.1.0 버전 때는 도저히 못써먹을 물건이었던 `Android Studio`는 0.4.0 버전이 되더니 괜찮게 써먹을 수준이 되었다. (사실 내가 `IntelliJ`를 알았었느냐 몰랐었느냐의 차이일지도 모르겠다.) 여기서는 잘 돌아가겠지 싶었다. Gradle이라는 빌드 자동화툴 때문에 기존 소스를 `Android Studio`용으로 바꾸는 데에 애를 좀 먹었지만 어떻게든 변환에 성공했고, `dependencies` 목록에 다음과 같이 추가한 후 빌드해보았다. 

{% highlight text %}
dependencies
{
        compile 'com.actionbarsherlock:actionbarsherlock:4.4.0@aar'
        compile 'com.android.support:support-v4:13.0.+'
}
{% endhighlight %}

그런데 어랍쇼, 또 aapt.exe 에러가 뜬다.

![엉엉엉엉 ㅠㅠㅠㅠㅠㅠㅠㅠ][image2]

`Stackoverflow.com`을 아무리 뒤져봐도 aapt.exe 에러는 xml 관련 에러라고만 뜨고 정확히 무슨 파일의 뭐가 잘못됐다는 이야기는 나오지 않았다. 다른 오류인 aapt.exe 파일을 찾을 수 없다는 에러나 에러코드 1과 함께 종료되는 문제의 해결책이 주로 나왔지, 내가 겪고 있는 에러코드 `-1073741819`에 관련되서는 딴소리만 잔뜩 쓰여있었다. xml 에러라는 것은 감을 잡았는데 정확히 뭐가 에러일까 싶어 계속해서 구글링을 하다가 보니 찾게 된 페이지는 [안드로이드 오픈소스 프로젝트 이슈트래커 페이지][url1]였다. 

> Hi,
>
> I got this problem when I added the Faceebook SDK project as a library to my project.
> In my case the problem was in the resource file styles.xml.
> I had a resource defined this way:
>
> <style name="my_style" >
>   <item name="android:id">@+id/my_style</item>
> </style>
>
> In order to use this style, don't set an id for it, just use R.style.my\_style in a Java class, or @style/my\_style in a xml file. If consequently you define an id property, the project resources will be mixed up and the R.java class will not be generated.
>
> Hope it helps!
>
> <footer>Compile error: Error executing aapt: Return code -1073741819</footer>

`style.xml` 파일에서 id값을 지정하지 말라고 제시되어있었다. 그리고 예전에 layout에서 id를 지정해놓는게 귀찮고 보기 안좋아서 `style.xml` 파일에 전부 입력해놓고 layout에서는 스타일 지정만 해놓은게 생각났다. 제시된 대로 `style.xml` 파일에서 id값을 제거하고 layout에서 집어넣었더니 드디어 빌드 성공.

해결책을 여기에 작성해놓는데는 얼마 걸리지 않았지만 나는 이 에러 때문에 거의 3시간을 날려야만 했다. 모두가 나같은 시간낭비는 하지 말았으면 하는 마음으로 적어본다.

[image1]: http://i.imgur.com/0R7TqNn.jpg
[image2]: http://i.imgur.com/3mHJvAv.jpg

[url1]: https://code.google.com/p/android/issues/detail?id=42752#c2