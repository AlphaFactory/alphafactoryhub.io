---
layout: post
title: 안드로이드 파편화와 iOS 파편화의 차이
date: 2012-11-02 15:00
image: http://i.imgur.com/mUOVUMO.jpg
thumbnail: http://i.imgur.com/mUOVUMOb.jpg
---
파편화에는 두가지 종류가 있다. 하나는 하드웨어의 차이(디스플레이 사이즈, 램 용량, CPU 클럭 등)에서만 기인하는 파편화가 있고, 또 하나는 소프트웨어적인 차이에서 기인하는 파편화가 있다.

하드웨어적 차이에서만 기인하는 파편화는 개발사의 노력으로 충분히 해결이 가능하다. 기껏해야 태블릿 모드를 집어넣거나 태블릿 전용 앱을 따로 개발하는것 정도면 충분. 안드로이드도 4.0 ICS 올라오면서 FragmentActivity라는 걸 넣게 되고, 이는 개발자로 하여금 손쉽게 디스플레이 파편화를 최대한 막아주는 역할을 하게 되었다.

덕분에 실제로 요즘은 이 파편화가 거의 없다. iOS는 애초에 적었고. 물론 개발자가 RelativeLayout(상대적 레이아웃)이 아니라 AbsoluteLayout(절대적 레이아웃)으로 짜는 것 같은 변태짓만 안한다면.

iOS는 애초에 해상도 2배 정책으로 이 파편화가 명시적으로 보이는 것을 최대한 막았고, 이번에 아이폰 5 나오면서도 SDK 적으로 최대한 이 문제를 없애도록 했다. 물론 게임의 경우에는 아직 레터박스 문제가 남아있지만, 이건 업데이트가 늦는 개발사 문제지 iOS의 문제라고 치부하기엔 무리가 있다. 왜냐하면 iOS 개발 레퍼런스 문서에는 에는 분명히 해결방법이 적혀 있기 때문이다.

다음으로 소프트웨어적 차이에서 기인하는 파편화를 살펴보자.

iOS도 버전이 올라가면서 API함수가 deprecated화 되거나 아예 없어지거나, 결과값이 다르게 되는 경우도 있지만, 버전마다 매우 고유하고 명시적이다. 즉, 레퍼런스를 그대로 따라가기만 한다면 그다지 문제가 되지 않는다는 소리이다. 아이폰 4이냐, 아이팟터치 5세대냐, 이런거 차이 없이 대부분의 에러는 iOS의 버전에 따르게 마련이다.

그런데 안드로이드는 제가 아래에서 썼듯이, 기기마다 이 오류가 다 다르다. 삼성은 자회사 휴대폰들 사이에서도 파편화가 일어나기도 하며, 어떤 휴대폰은 화면 방향 얻어낼 때 나오는 함수의 결과값이 반대로 나오기도 한다. 이건 iOS와는 달리 "기종마다" 다 다르기 때문에 다 잡아낼 수 있는 방법이 없다. 같은 버전인데 기기마다 다른 문제가 발생하는게 안드로이드이다.

안드로이드가 오픈소스인 이상 당연한 일이고 이건 구글이 노선을 바꿔 윈도우폰같은 전략으로 가지 않는다면 절대로 완벽하게 수정될 수 없는 오류중에 하나다.

시코를 비롯한 IT 커뮤니티에서는 이상하게 파편화 얘기 나오면 플래그쉽 안드로이드 폰 사라는 얘기가 나오는데, 이건 파편화를 "최대한 피하는" 방법이지 해결하는 방법이 아니다. 제발 구별 좀 하자.

PS) 아이러니한 건 세계에서 가장 잘 팔리는 안드로이드 폰인 삼성 갤럭시 시리즈는 개발자들 사이에서 파편화를 조장하는 폰으로 악명높다는 것. 다른 기기에선 다 되는데 갤럭시에서만 안되는 경우가 상당하다. 그중 가장 유명한 것은 SMS 메시지 프로바이더 문제로, 이건 같은 갤럭시 계열에서도 파편화가 있을 정도.
