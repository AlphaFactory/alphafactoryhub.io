---
layout: post
title: R.java 파일 생성 오류 해결책
date: 2013-07-19 08:55
thumbnail: http://i.imgur.com/GcDGvhWb.png
---
안드로이드로 개발하다보면 R cannot be resolved to a variable 오류를 상당히 많이 접한다. 아무리 안드로이드에 익숙한 개발자도 프로젝트 당 최소 한두번은 경험할 것 같다. 꽤 잦은 오류인데, 이유는 여러가지가 있다. 단순히 Clean을 하면 되는 문제부터 시작해서 프로젝트 전체를 뜯어고치거나 이클립스자체를 새로 설치해야 되는 경우도 왕왕 있었다.

그런데 최근 겪은 R.java 파일 생성 오류는 그 정도가 지독했다. 일단 초보들이 자주 실수하는 문제인 XML 파일명 혹은 XML 문법 오류는 아니었다. 모든게 제대로 되어 있었는데다, 어플 빌드까지 제대로 됐다. 어플빌드를 확인하고 나면 Clean을 눌러주는 버릇이 있었는데, 이렇게 Clean을 하고 나니 갑자기 익숙한 R cannot be resolved to a variable 오류가 났다.

XML의 모든 문법과 철자 하나하나까지 다 뒤져봤는데도 오류가 해결이 안되서, 결국 이클립스를 다시 설치했는데도 불구하고 여전히 해결이 안되는 것이다. 프로젝트의 src, res, libs 폴더를 제외하고 전부 날려버린 다음에 다시 import를 시켜도 여전히 에러가 있는 것이다.

도대체 뭐가 문제인지 답이 안나와서 조사를 좀 해봤더니, [stackoverflow.com에서 찾은 검색결과][url]에 따르면 ADT Rev. 22.0.0의 에러였다.

> I assume you have updated ADT with version 22 and R.java file is not getting generated.
>
> If this is the case, then here is the solution:
>
> Hope you know Android studio has gradle building tool. Same as in eclipse they have given new component in the Tools folder called Android SDK Build-tools that needs to be installed. Open the Android SDK Manager, select the newly added build tools, install it, restart the SDK Manager after the update.

빌드 툴이 새로이 생긴 듯 한데, 이게 없을 경우 R.java 파일이 제대로 생성되지 않는 오류가 있다고 한다. 해서, 실제로 Android SDK Build-tools를 설치하니 문제가 말끔히 사라졌다.

이 글을 읽는 R.java 생성 에러에 걸린 개발자라면, 일단 res폴더의 XML의 문법 오류와 XML 파일 이름 오류(소문자와 언더바 기호만 사용할 것)가 없는지 확인하고, 그래도 오류가 해결되지 않는다면 SDK Manager에서 Android SDK Build-tools가 설치되어 있는지 확인해 보길 바란다.

[url]: http://stackoverflow.com/questions/16642604/eclipse-error-r-cannot-be-resolved-to-a-variable