---
layout: post
title: 넥서스 7에 듀얼쇼크 3 연결해서 게임하기
date: 2012-10-18 16:36
image: http://i.imgur.com/NcRpNDf.jpg
thumbnail: http://i.imgur.com/NcRpNDfb.jpg
---
일반적으로 스마트 기기라고 하면 터치 인터페이스를 가진 게임만을 생각하기 마련이다. 앵그리버드와 같은 게임은 터치인터페이스가 괜찮은 경우이지만 2D 액션 RPG를 비롯해 상당수의 게임은 화면위에 있는 버튼 보다는 컨트롤러가 훨씬 더 편한 것이 사실이다. 무엇보다 터치 인터페이스는 화면의 감도가 문제가 되는 경우도 있어 미스컨트롤이 잦다. 특히나 FPS같은 경우에는 말이다.

넥서스 7을 구매하고 나서 게임 몇가지를 해봤지만 역시나 화면이 커지다 보니 대부분의 게임들이 터치 인터페이스가 커지고, 그 덕분에 영 게임의 조작이 힘들어졌다. 그래서 예전부터 생각만 해두고 있던걸 해본게 이거다.

플레이스토어에서 Sixaxis나 Dualshock라고 검색하면 두개의 결과가 뜬다.

![][image1]

왼쪽의 앱은 기기 지원여부를 확인하는 앱이고, 오른쪽의 앱은 실제 컨트롤을 위한 앱이다. (쉽게 말하면 오른쪽의 앱은 윈도우의 드라이버와 같은 것이라고 보면 된다.) Checker 앱에서 테스트를 해 보고, (Start버튼만 누르면 된다.) 앱이 기기를 지원하는게 가능한 경우 아래와 같은 창이 뜨게 된다.

![][image2]

이제 앱이 안내하는대로 Dancing Pixel Studios 홈페이지에 들어가서 SixaxisPairTool을 다운로드 받고, ([다운로드 페이지][sixaxispairtool_download_url]) 컴퓨터와 듀얼쇼크 3 무선 컨트롤러를 연결하여 SixaxisPairTool을 이용해 기기의 페어링 대상 블루투스 주소를 페어링할 기기의 것으로 바꿔준다. 다시 Checker앱을 실행하고 Start 버튼을 누른 후, 듀얼쇼크 3의 전원을 켜면 자동적으로 페어링이 된다. 버튼을 눌렀다 뗏다 했을때 Checker앱에서 반응을 보이면 (아래로 쭉 로그가 뜬다.) 성공적으로 페어링을 한 것이다.

여기까지 성공했다면 제대로 작동이 되는것이니, Sixaxis Controller(참고로 아쉽지만 유료앱이다.)를 구매하여 설치 후 몇가지 설정을 해 주면 사용할 수 있다.

<iframe class="ybframe" src="http://www.youtube.com/embed/-6DIkERHIkA" frameborder="0"></iframe>

<iframe class="ybframe" src="http://www.youtube.com/embed/FRMWttBh-L0" frameborder="0"></iframe>

듀얼쇼크3를 활용하고 있는 지금은 마치 콘솔 게임기 하나 산 듯한 기분이다. Sixaxis Controller앱은 특정좌표의 터치 반응도 버튼에 할당할 수 있으니 사실상 지원하지 못하는 앱이 없다고 보면 된다. 딱히 필요성을 못느껴도 기존에 플레이스테이션 3를 가지고 있는 사람들이면 한번쯤 해봐도 재미있을 것이다.

[sixaxispairtool_download_url]: http://www.dancingpixelstudios.com/sixaxiscontroller/tool.html
[image1]: http://i.imgur.com/bjmAJmg.png
[image2]: http://i.imgur.com/psgzIXw.png