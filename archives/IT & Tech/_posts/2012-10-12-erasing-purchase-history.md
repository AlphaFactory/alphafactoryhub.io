---
layout: post
title: 플레이 스토어 구매내역 삭제가 가능해질 듯?
date: 2012-10-12 09:50
thumbnail: http://i.imgur.com/G18xcqcb.png
---
![어플리케이션 구매내역을 삭제하는 모습][image1]

> Now, users will be able to remove apps individually or together in a batch from that list by tapping a small strikeout symbol, so you can more effectively manage your app history.
>
> <cite>[phoneArena.com][url1]</cite>

남보기 부끄러운 어플 설치하시고 나서 구매내역 삭제가 안되어 곤혹스러우셨던 분들은 이제 구매내역 삭제가 가능해질 것으로 보인다. 기사에 따르면 아마도 구글 플레이 스토어의 다음 업데이트에서 가능해 질 것이라고 한다. 댓글란을 분주하게 만들었던 요소중 하나가 드디어 줄어들거나 아예 사라질 것으로 생각된다.

물론 앱 목록을 삭제했다고 해서 실제 구매한 사실 자체가 사라지거나 하진 않을 것이므로 유료앱을 앱 리스트에서 삭제한다고 하더라도 재구매하거나 하는 일은 없을것으로 보인다.

[image1]: http://i.imgur.com/G18xcqc.png
[url1]: http://www.phonearena.com/news/Google-Play-updated-to-allow-removal-of-apps-on-All-list_id35438