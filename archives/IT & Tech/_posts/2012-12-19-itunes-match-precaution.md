---
layout: post
title: iTunes Match 쓰기 전에 주의하자
date: 2012-12-19 23:32
image: http://i.imgur.com/d5D4lAc.png
thumbnail: http://i.imgur.com/d5D4lAcb.png
---
해당 애플 디바이스가 아이튠즈 매치를 Subscribe한 계정이 아닌 다른 계정에 이미 등록되어 있을 경우, 그 디바이스로는 등록일자로부터 90일이 지나야 아이튠즈 매치를 Subscribe한 계정에 다시 등록할 수 있다.

덕분에 나도 80일간 아이튠즈 매치를 사용 못하게됐다. 내년 3월 중순이나 되어야 재등록이 가능할듯…. 슬프다. 제대로 안 알아본 나도 문제지만, 그걸 왜 90일동안이나 묶어놓고 있는지도 참 이해가 안가는 처사긴 하다. ㅠㅜ
