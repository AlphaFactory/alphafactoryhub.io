---
layout: post
title: 워드프레스 기본 개념 익히기
date: 2013-10-10 13:54
thumbnail: http://i.imgur.com/9piBf95b.png
---
워드프레스는 국내의 블로그들과는 컨텐츠를 생산하는 방식이 사뭇 다르다. 글을 쓰는 방법이나, 사진을 삽입하는 방법, 글과 페이지의 차이, 댓글 검토 등 네이버나 티스토리 등의 블로그와는 상당한 차이점이 존재하는데, 이를 알아보도록 하자.

##1. 글

글을 쓸 때, 네이버 블로그를 비롯한 다른 블로그에서 게시글을 쓰다가 온 사람이라면 알아두어야 하는 것이 있다. 바로 HTML 태그를 정확하게 알고 사용해야 한다는 것.

네이버 블로그는 문단을 바꿀 때 &lt;BR&gt;태그나 &lt;DIV&gt;태그를 사용하는데, &lt;BR&gt;태그는 몰라도 우선 &lt;DIV&gt;태그는 영역을 나누는 태그이다. 영역을 나눈다는 점에서 "문단을 나눌수도 있지 않나" 할 수 있지만, 이는 왼쪽 메뉴와 오른쪽 내용을 나누는 등 화면의 전체적인 영역을 나눌 때 사용하는 것이지 문단을 나누는데 사용하는 태그가 아니다.

그럼 문단을 나눌 때 사용하는 것은 무엇인가. &lt;P&gt; 태그이다. 워드프레스는 줄 나눔을 위해 Enter를 칠 때 &lt;BR&gt;태그가 아니라 &lt;P&gt;태그를 입력하게 되며, &lt;BR&gt; 태그를 입력하려면 Shift + Enter를 눌러야 한다. 즉, 강제 줄바꿈을 위해 사용하는 &lt;BR&gt;과 문단 나눔을 위해 사용하는 &lt;P&gt;태그의 사용 목적이 분명하게 나뉘어져 있다는 것이다.

네이버 블로그를 쓰던 사람이 워드프레스에 오게 되면 습관처럼 Enter를 두번 입력해 문단을 나눠 표시하려 하는데, 그럴 필요 없다. 문단을 나눌 때는 반드시 Enter를 한번만 입력하자. CSS에 P태그에 관련되어 속성이 정의되어 있으므로 굳이 Enter를 두번 누르지 않아도 문단 사이에 어느정도 간격이 확보되어 있다. 굳이 같은 줄간격을 유지하면서 강제개행이 필요하다면 Shift+Enter를 치도록 하자.

##2. 미디어

미디어라 함은 사진이나 음악, 동영상 등을 이야기하는데, 주로 블로그에 사용하는 것은 사진이므로 사진부터 이야기하자.

글에 사진을 입력하는 것 등은 워드프레스 역시 자유롭다. 네이버와 같은 한국 블로그 시스템과 다른 점이 있다면 "여태까지 자신이 업로드한 미디어를 총체적으로 관리할 수 있다"는 점이겠다.

![워드프레스 미디어 추가 화면][image]

같은 이미지를 넣기 위해 두번 업로드하거나 이전 포스트에 가서 주소를 복사해올 이유가 없으며, 각 이미지의 제목, 캡션, 대체 텍스트, 설명 등, &lt;IMG&gt; 태그 관련된 부속 속성들도 관리 가능하다. 캡션의 경우는 Shortcode라는 일종의 워드프레스 전용 태그를 통해 입력되는데, 이는 캡션을 입력하기 위해 일일히 사진 아래에 글을 쳐 넣어야 하고 폰트도 일일히 수정해줘야 하는 국내 블로그들과는 큰 차이점이다. 사진과 그에 따른 캡션을 많이 입력하는 사람이라면 워드프레스만큼 편한 블로그도 없을 것이다.

이와 비슷하게 비디오나 음악도 업로드 및 관리가 가능하다. 최근 3.5 버전에서 미디어 관련 PHP 모듈이 강화되어있는 것을 보면 사진 이외의 미디어를 강화하려는 움직임이 보인다. 허나, 비디오는 호스팅 업체를 사용할 경우 스트리밍 때문에 트래픽을 버틸 수가 없으므로 Vimeo나 Youtube 등의 서비스를 이용하는 것이 좋으며, 음악은 Soundcloud와 같은 서비스를 이용하는 것이 좋다.

##3. 페이지

워드프레스는 일반적인 블로그 포스트, 즉 글과는 달리 카테고리나 태그로부터 분리된 특별한 "페이지"를 작성할 수 있는 기능이 있다. 이 페이지는 블로그 첫 화면을 특별하게 꾸민다거나, 연락용 폼(Contact Form)을 추가해놓는다거나 하는 용의, 즉 정적이고 일관적인 글이 필요한 경우에 사용한다. 자기 소개나 포트폴리오 용으로 사용하는 경우가 많다.

항상 전면에 보여야 하는 글이 있다면 페이지로 작성하는 것을 추천한다.

##4. 댓글

워드프레스는 댓글 관리하기가 더욱 편리하다. 국내에서 비슷한 서비스가 있다면 티스토리일 것이라 본다. 워드프레스는 댓글 승인 시스템을 가지고 있다. 댓글을 입력하면 일단 관리자가 확인 한 후에 블로그에 표시되는 것을 허락하는 댓글 검토 기능이 대표적인 기능인데, 일단 댓글을 표시해놓고 사후에 신고해야만 하는 국내의 각종 블로그들과는 꽤 다르다. 스팸으로 댓글란이 더러워지는 것을 막을 수 있으며, 분쟁 유발을 원치 않은 게시물에 분쟁성 댓글이 달리는 것을 막을 수도 있다.

물론 이 기능은 꺼놓을 수도 있다. 단순히 스팸 때문이라면 Akismet 등의 플러그인을 사용하면 되므로 스팸쪽은 걱정할 필요가 없다. 일단 승인한 댓글이라도 검토중으로 되돌리거나 하는 것도 가능하다. 이외에 휴지통, 스팸으로 설정해놓고 일정시간 지우지 않는 기능도 있어 IP차단이나 특정 키워드 차단에 여러모로 도움이 된다.

댓글을 승인하지 않은 상태로 이에 대한 답글을 달면 이전 댓글은 여전히 승인되지 않은 상태이므로 다른 사람에게는 보이지 않으므로 주의해야 한다.

[image]: http://i.imgur.com/9piBf95.png