---
layout: post
title: 블로그 테마에 Solarized 컬러 적용
date: 2014-07-20 01:50
tags: [diary, jekyll, theme]
---
최근 이것저것 알아보던 중에 Solarized라는 색상 조합에 끌리게 되었다. 사실 정확하게는 Sublime Text를 이용해서 뭔가 작업을 하던 중에 눈이 좀 아프고 가독성도 영 좋지 않은 것 같아 색깔을 좀 바꿔보려다가 우연히 Solarized Scheme을 적용해보고 놀랐던 것이다. 단어 자체는 디자인 관련 사이트에서 꽤나 많이 보던 단어라 익숙했는데 이런 색상 조합일 줄은 생각하지 못했다.

차분하지만 강조 색상은 확실하게 드러나는 색 조합이었는데, 눈도 편해서 보기도 좋았다. 당장 블로그에 적용해보고 싶어서 예제로 쓸만한 테마가 없는지 확인해봤고, 그 결과 찾은 테마가 [Matt Harzewski라는 사람이 만든 Solar][link1]였다.

![Solar 테마 예시 스크린샷][image1]

그리고 이것의 소스파일을 좀 여러모로 수정하고 내 블로그에 맞게 바꾸었다.

좀더 미니멀한 느낌이 되어서 마음에 들긴 하지만, DISQUS에 CSS를 마음대로 적용하기가 힘들어서 블로그 글과 댓글부분의 일체감이 부족하다는게 단점이다. 수정할 수 있으면 좋을텐데 2012년 이후 DISQUS가 개편한 이후로는 잘 안된다는 것 같다. 다른 댓글 시스템을 알아봐야 하나 싶지만 일단 DISQUS를 대체할 만한 댓글시스템은 내가 아는 내에선 별로 없으니 한동안은 이대로 써야겠다.

[image1]: https://camo.githubusercontent.com/6f968e8ecb93a5f49e5408fdba6770a18b68f851/687474703a2f2f692e696d6775722e636f6d2f556e6e52686b742e706e67

[link1]: https://github.com/redwallhp/solar-theme-jekyll