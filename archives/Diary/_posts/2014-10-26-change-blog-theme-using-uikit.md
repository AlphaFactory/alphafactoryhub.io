---
layout: post
title: UIKit을 이용한 블로그 테마
date: 2014-10-26 18:50
---
웹 상에서의 UI Framework에 관심이 생겼다. 예전에 Twitter Bootstrap을 이용해서 잠깐 블로그 테마를 만들곤 했었는데, 이유는 모르겠으나 별로 써먹기 좋다는 생각은 들지 않았다. 사실 Bootstrap이 쓰기 불편하다기보다 그 때는 Liquid를 사용하여 Jekyll 블로그를 제대로 커스텀할 수 없었기 때문이라고 보는 것이 더 타당하겠다.

최근, 관련 구글링 중에 UIKit이라는 새로운 녀석을 알게되었다. 그리고 이 놈을 적극적으로 사용해보기로 했다. 가장 큰 이유는 UIKit은 Markdown 편집기를 가지고 있다는 것이다. Jekyll 블로그도 좋긴 하지만, 글 하나 포스팅하기 위해서 항상 블로그 프로젝트 전체를 build 해야하는 것이 조금 마음에 안들었었기 때문에 jekyll의 Markdown 양식을 그대로 따와서 Java를 이용해 나만의 CMS를 만들어볼까 하는 생각을 하고 있었는데, Markdown 편집기를 내장하고 있다면 그 일이 한결 수월해질 것이라는 생각이 들었다.

하지만 있다고 해서 맘대로 써먹을 수 있는 것은 아니다. 웹 개발을 할 수는 있으나 안드로이드만큼 자유자재로 뭔가 하기에는 아직 많이 부족하기 때문에 좀 연습이 필요했다. 해서 UIKit을 이용해 무언가 만들어보기 전에 일단 블로그부터 손대보기로 했다.

완전히 내 생각대로 만들지는 못했으나, 일단 어느 정도는 모습을 갖춰가는 것 같아 일단 업로드 하였다. UIKit이 Responsive Design을 지원하지만 내가 수작업으로 수정하는 과정에서 모바일 화면이 일부 이상하게 되었지만, 일단은 업로드해도 괜찮을 것 같다는 생각이 들어 업로드 해 본다.

Off-Canvas 기능도 아직 넣지 못했고 블로그 글 목록 부분도 UIKit의 Component를 이용해 뭔가 수정해보고 싶지만 아직은 딱히 떠오르는 디자인이 없어 내버려 두기로 했다.

블로그 글도 열심히 좀 쓰고싶은데 요즘은 쓸만한 글도 없고 영 시간이 나지 않아 부담스럽다. 조만간 안드로이드 강좌 글을 쭉 한번 써볼까 하는 생각은 있지만 그것도 생각일 뿐이고 실행할 여유가 없다.