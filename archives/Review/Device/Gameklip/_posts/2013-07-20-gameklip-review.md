---
layout: post
title: Gameklip™ 사용기
date: 2013-07-20 09:50
image: http://i.imgur.com/6ES6SPC.jpg
thumbnail: http://i.imgur.com/hQ3x8Ghb.jpg
---
사실 이 플라스틱 클립을 내가 이제서야 막 안것은 아니다. 작년부터 알고는 있었지만 국내로 국제배송이 되지 않는데다가 누가 국내로 수입해주는 것도 아니었기 때문에 물건너에는 이런게 있다는 것만 알고 있었을 뿐이다. 헌데, 최근 정말 운좋게 국내에 배송대행을 하여 이 제품을 사용하고 있던 사람 중 한명이 중고로 판매하는 것을 보고 바로 연락, 그렇게 손에 넣을 수 있었다.

예전에도 소개했지만 안드로이드 폰에는 소니 플레이스테이션 3의 게임패드인 듀얼쇼크 3와 폰을 블루투스로 연결하게 도와주는 Sixaxis Controller라는 앱이 있다. Gameklip은 애초에 만들때부터 이 Sixaxis Controller 앱에 영감을 받아 제작되었으며, 공식 사이트에서도 해당 앱을 사용할 것을 권장하고 있다.

> After flipping through pages of cheap gimmicky Bluetooth controllers, I stumbled across the Sony Dualshock3 controller and **the Sixaxis app**. The Sixaxis app allows many Android phones to connect to a Playstation 3 controller to give native analog input and emulate touch and hardware controls. I thought I had found my answer, but where do I put my phone if the controller is in my hands? Enter the GameKlip™

본인의 경우 넥서스 7용 게임클립을 중고로 구매했는데, 넥서스 7용 마운트(마운트라고는 하지만 사실 그냥 케이스정도라고 보면 된다)는 떼어버리고, 지하철 노점에서 산 5천원짜리 젤리케이스를 부착하였다. 그 결과물이 위 사진이고, 휴대폰과 듀얼쇼크3를 장착한 모습은 다음과 같다.

![듀얼쇼크 3와 갤럭시 노트 2를 게임클립을 이용해서 연결한 모습. 게임하는데 지장이 있는 무게는 아니지만, 무게중심이 앞으로 쏠리게 되어 게임하다가 내려놓고 거치하기는 좋지 않다.][image1]

Sixaxis Controller앱의 사용법은 [예전에 넥서스 7을 가지고 있을 때 썼던 글][url]에서 언급했으니 생략하기로 하고, 해당 앱의 조금 특별한 기능을 소개해보고자 한다.

Sixaxis Controller앱은 게임패드 에뮬레이팅 뿐만 아니라, 버튼에 터치영역을 할당할 수도 있다. 우선 주 게임화면을 스크린샷으로 찍고, Sixaxis Controller 앱에서 Menu > Preferences > Touch Emulation > Edit Touch Profiles에 들어간다. 그럼 편집화면이 나오는데, Menu > Change Background를 눌러 스크린샷으로 찍었던 게임화면을 불러와, Menu > Add Button을 통해 듀얼쇼크에 해당하는 버튼을 추가하고 터치영역에 위치시킨 후, 프로필을 저장하고 빠져나온다. 메인 화면에서  Active Touch Profile을 방금 전 저장한 프로필로 바꿔주면 끝이다.

![O2Jam Analog를 PSP DJMax Portable처럼 즐기기 위해 위와같이 버튼배치를 해 보았다.][image2]

이를 본격적으로 활용하는 예시를 보자. 아래 동영상은 직접 촬영하고 편집했다.

<iframe class="ybframe" src="http://www.youtube.com/embed/mmxYLXNFgoE" frameborder="0"></iframe>

게임하기는 정말 편하다. 단점이라면 바깥에 가지고 나가서 돌아다니기에 듀얼쇼크 3와 게임클립은 좀 크다는 것 정도일까? 그리고 플라스틱 클립 치고는 가격이 지나치게 센 편이라는 것 또한 조금 불만이다. 국내에 중고로 돌아다니는 가격이 거의 4~5만원이나 하니... 그래도 만족도가 꽤 높기에 나로서는 마음에 드는 구매였다.

[image1]: http://i.imgur.com/hQ3x8Gh.jpg
[image2]: http://i.imgur.com/w3Ts4m3.png
[url]: {% post_url 2012-10-18-connect-dualshock3-to-nexus-7 %}