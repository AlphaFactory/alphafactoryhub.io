---
layout: post
title: HD5 Clear 리뷰 - 번외편
date: 2010-09-26 21:33
image: http://i.imgur.com/6wCkWh9.jpg
thumbnail: http://i.imgur.com/neOY7vFb.jpg
---
![적용후 사진. 기본 기능 뿐만 아니라, 따로 작업관리자를 로드하거나 Windows CE로 나가는 기능도 추가했다.][image1]

HD5 Clear 체험단 활동중인 아르파입니다.

ResMondrian.dll, ResCasual.dll을 통한 테마 변경이 너무 제한적이고 노력이 많이 들어가야 하기 때문에 좋은 런쳐 프로그램을 소개해 드립니다.

전에도 말씀드렸던 WolfNCU라는 프로그램으로, 원래는 Win CE 기반의 네비게이션용 런쳐를 위해 만들어진 프로그램이나, 최근에는 PMP에도 자유로운 적용이 가능해 개인적으로 많이 활용해 왔습니다.

허우행 님이 제작하셨으며, 스킨 제작 매뉴얼 및 기타 자세한 사항은 [홈페이지][wolfncu_url]을 참고해 주세요.

이미지 + 버튼 기반의 런쳐이기 때문에 누구나 쉽게 만들수 있습니다.

HD5에 WolfNCU를 제대로 적용하기 위해서는 다음과 같은 절차를 거쳐야 합니다.

1. WolfIntro 프로그램을 Launcher.exe로 이름을 바꾼다.
2. MMC Card\HD5\Launcher.exe파일을 다른 이름으로 바꾼다.
3. WolfIntro 프로그램에서 설정으로 들어가 WolfComAgent 부분에 원래의 HD5 Launcher(MMC Card\HD5\Launcher.exe의 이름바꾼 것)을 지정하고 Navigation부분은 WolfNCU.exe 파일을 지정한다. Order는 둘다 1로 바꾸고 Delay는 0, 그리고 둘다 Run에 체크한다.
4. Launcher.exe로 이름을 바꾼 WolfIntro 프로그램을 WolfNCU와 관련된 파일들과 함께 통째로 MMC Card\HD5 폴더 내에 복사한다.

기존의 Launcher.exe를 실행하고 그 위에 WolfNCU.exe를 또 실행시키는 형태입니다.

기존의 Launcher.exe를 실행하지 않을 경우 볼륨 및 전원키가 제대로 작동하지 않기 때문에 우선은 실행해 주어야 합니다.

나중에 WolfNCU에서 작업관리자를 실행해 Launcher를 종료시켜 주시면 됩니다.

이상으로 9부를 마칩니다.

[wolfncu_url]: http://www.wolfzone.co.kr/
[image1]: http://i.imgur.com/neOY7vF.jpg