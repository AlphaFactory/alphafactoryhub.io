---
layout: post
title: HD5 Clear 리뷰 - 음악에 관하여
date: 2010-09-02 01:03
image: http://i.imgur.com/6wCkWh9.jpg
thumbnail: http://i.imgur.com/qKiDC5kb.jpg
---
안녕하세요.

HD5 Clear 체험단의 아르파입니다.

간단한 개봉기와 외형은 지난번 리뷰에서 살펴봤으니, 이번 리뷰부터는 슬슬 PMP로써의 본연의 기능과 활용성에 대하여 천천히 알아보도록 합시다.

이번편에서는 HD5 Clear의 음악 재생기능에 대해서 모든것을 알아볼 생각입니다.

##1. 인터페이스 (Interface)

![기본 화면(파일 목록)][image1]

![전체 화면 (첫번째 사진에서 앨범아트 부분을 터치하면 위 화면이 된다)][image2]

기본적인 인터페이스는 위의 사진과 같습니다.

인터페이스의 조작성에 대해서 말하자면, 다른 PMP와 별 다를 것 없이 무난합니다. 물론 음악을 주로 듣는 전용 MP3 플레이어의 경우에 비해서, PMP라는 기기는 화면도 크고 기기 자체의 몸집도 큽니다. 따라서, - 물론 기기에 익숙해 진다면야 이런 점도 커버는 가능하지만 - 아무래도 전용 MP3 플레이어보다 조작성은 떨어지는 것이 사실이죠.

그렇다면 기능성에서는 어떨까요?

![HD5 Clear 음악 플레이어의 여러가지 곡 선택 방법][image3]

우선 제가 가장 좋은 점으로 뽑고 싶은 점은, 파일 목록에서 음악 파일 전체보기, 즐겨찾기 등록 목록보기, 탐색기, 외장 메모리 탐색기 사이를 원터치로 자유로운 이동이 가능하다는 점입니다. 이는 Cowon S9에서도 있었던 기능인데, S9의 경우에는 원터치로 이동하는 것은 불가능했죠. (물론 UCI를 통해 얼마든지 구현 가능하지만 관련 기능을 가진 UCI는 아직 없습니다. 즐겨찾기와 북마크는 가능했지만요.)

헌데 북마크 목록을 보는 버튼을 첫번째 화면이 아니라 두번째 화면에 집어넣은 이유는 솔직히 좀 의문입니다. 즐겨찾기 등록/목록보기 뿐만 아니라 북마크까지는 첫번째 화면에 넣어주는게 맞지 않았을까요.

재생, 스킵, 이전곡/다음곡 등의 기본적인 조작 버튼이나 구간반복과 같은 어학용 기능도 잘 작동했습니다.

![아이팟 터치의 유명한 기능 중 하나인 태그 정렬 기능. HD5에는 이 기능이 없다. 이미지 출처는 http://dolgubooki.com/70033967001][image4]

아쉬운 점이 한가지 있다면, 음악 파일을 태그별로 정렬해 주는 기능의 부재입니다. 이 기능은 애플사의 아이팟이 매우 유명한데, 현재 제가 사용중인 MP3인 Cowon S9에도 이 기능이 있습니다. 또한 최근의 기기에는 이 기능이 거의 빠지지 않고 등장하죠.

저의 경우는 PMP든 MP3든 간에 즐겨찾기나 재생목록을 만들지 않고 파일 탐색기에서 직접 재생시키기 때문에 문제가 되지 않을 지 몰라도, 태그별 정렬 기능을 사용해 왔던 사람이라면 HD5가 불편하게 느껴질 수도 있겠습니다.

##2. 스펙 재현

![빌립 공식 홈페이지에 있는 HD5의 오디오 재생 스펙][image5]

음악 파일 포맷에 대한 재생 테스트를 간단히 해 보았습니다.

MP3, AAC(\*.m4a), OGG, WAV, WMA, FLAC, APE에 대해서 진행하였으며, PCM/ADPCM의 경우는 음악감상에 있어서 거의 쓰이지 않는 파일이라 제외하였습니다.

![][image6]

실험 결과는 모두 정상적이었습니다.

개인적으로 혹시나 하는 마음에 ALAC(m4a파일 중 무손실 압축 방식. Apple Lossless Audio Codec)파일에 대해서도 재생 테스트를 해 보았지만 스펙에 표기가 되지 않은 것으로, 역시 재생되진 않았습니다.

즉, 스펙상의 성능을 제대로 다 보여주고 있다고 보면 되겠군요. :)
배속재생이나 SRS HD WOW의 적용 등에 있어서도 여러가지 테스트를 거쳐 보았는데, 모두 정상적으로 작동되었습니다.
##3. 음악 감상 - 음질과 음장에 관하여 -
음악감상을 주로 하시는 분이나, 음악에 관한 한 매니아이신 분들이 가장 중요하게 생각하는 것은 바로 기기 자체가 뽑아내는 음질와 그 기기가 가진 음장 시스템이 최대 관심사일 텐데요.

이에 대한 리뷰를 들어가기 전에 우선 많은 분들이 헷갈려 하실 음질과 음장에 대한 차이점부터 짚고 넘어갑시다.

![음질에 대한 이미지에의 비유][image7]

![음장에 대한 이미지에의 비유][image8]

위의 이미지는 제가 따로 처리한 이미지들 입니다. 대충 느낌이 오십니까?

음질이라 함은 음악 파일 자체가 가지는 퀄리티(Quality)입니다. 화질이 좋지 않은 사진은 왼쪽의 사진처럼 픽셀이 뭉개지고 형태가 뚜렷하지 못합니다.

음장이라 함은 음악 파일에 따로 후처리를 통해 효과(Effect)를 주는 것을 말합니다. 위의 사진은 각각 동일한 화질의 이미지에 뽀샤시와 아웃포커싱의 효과를 주어본 것입니다. 화질이 동일하기 때문에 중심 물체의 형태는 뚜렷하고 선명합니다. 하지만 이미지가 우리에게 주는 느낌은 확연히 다릅니다.

![한시대를 풍미했던 구형 MP3 플레이어들. 같은 MP3 파일이라면 기기가 어떻든 현재와 비교해도 음질 차이는 거의 없다. 이미지 출처는 중고나라.][image9]

예전의 카세트 테이프나 CD 플레이어, 좀더 과장하자면 LP 레코드판이 흥하던 시절에는 기기가 내 주는 음질이라는 것이 중요했습니다. 하지만 지금은 음악 감상에 있어서의 주요 수단은 MP3파일과 같은 "디지털 신호인 1과 0의 집합체로 된 파일"로 존재하고 이를 재생하는 알고리즘 또한 거의 대부분 "동일"합니다.

즉, 무슨 말이냐 하면 기기 자체가 음악을 재생하는 기능에 있어서, 뭔가 하드웨어적 결함이 있지 않는 한 음질에 관해서는 어떤 기기를 사용하든 거의 똑같다는 말입니다. 그것이 같은 MP3 파일이라면 말이죠.

따라서 이번 리뷰에서는 HD5의 음질이 아니라 "음장"에 대해 알아볼 것입니다. 앞에서도 말했듯이 최근의 기기에 있어서 음질을 좌우하는 것은 기기 자체의 성능이 아니라 음원 파일 자체이기 때문입니다.

그럼, HD5 Clear가 탑재하고 있는 음장 시스템에 대해서 알아봅시다.

![][image10]

SRS는 1993년에 설립된 회사 SRS Labs의 독자적인 음장 시스템으로, SRS(Sound Retrieval System)는 "음원 복원 시스템"을 의미하는 영어 약자입니다. SRS 음장은 세계 3대 음장 시스템 중 하나로써 돌비 사운드, Q사운드와 어깨를 나란히 하고있죠.

SRS WOW HD의 특징을 뽑자면,

- Immersive 3D Audio - 온몸을 감싸는 듯한 3D 오디오
- Improved Bass - 베이스 향상
- Elevated Sound - 사운드 상향 조정
- High Frequency Clarity - 고주파수 대역의 선명도

가 되는데, 가장 특징으로 삼을 만한 것은 역시 1번의 특징이 되겠습니다.

![HD5 Clear가 가진 SRS WOW HD와 음장 조정 및 설정 인터페이스][image11]

위의 사진에 따르면 HD5 Clear에 장착된 SRS WOW HD를 설정하기 위해선 6가지의 항목을 조정하는 것으로 보입니다.


<dl class="dl-horizontal">
  <dt>음장설정</dt>
  <dd>SRS 설정 이전의 이퀄라이징 (주파수 대역별 강조 정도 설정) 을 담당하는 항목입니다. Normal, Classic, Pop, Rock, Jazz, WOW 의 6가지를 제공합니다. WOW 항목으로 설정했을 때만 SRS WOW HD 음장 시스템의 적용이 가능합니다.</dd>
  
  <dt>SRS</dt>
  <dd>3D 표현의 정도를 설정하는 것으로, 숫자가 클수록 좀더 웅장한 느낌이 듭니다.</dd>
  
  <dt>Focus</dt>
  <dd>음의 초점을 맞추는 기능입니다. 다시 말하자면, 음을 선명하게, 혹은 둔탁하게 들리게 합니다.</dd>
  
  <dt>TruBass</dt>
  <dd>SRS의 주요 기술 중 하나로 가상의 베이스 우퍼 스피커를 듣는 듯한 느낌을 줍니다. 저음성향입니다.</dd>
  
  <dt>High</dt>
  <dd>잘못된 명칭입니다. SRS WOW HD는 SRS, Focus, TruBass, Definition, WOW의 5가지 설정 기능을 갖습니다. 이는 High가 아니라 정확하게는 Definition, 즉 음의 고음해상도를 높이는 역할을 맡습니다.</dd>
  
  <dt>스피커</dt>
  <dd>위에서 말했듯이 잘못된 명칭입니다. 원래의 명칭은 WOW로 추정되는데, 이는 음의 수평길이를 조정하는 것으로 알고 있습니다.</dd>
</dl>

해석의 잘못인지 아니면 뭔가 착오가 있었는지는 모르겠는데, 빌립 정도 되는 회사에서 SRS WOW HD의 설정창을 이렇게 허술하게 만들줄은 몰랐습니다. 좀 당황스럽긴 한데요, 이는 차후 펌웨어를 통해 얼마든지 수정될 수 있는 문제이므로 딱히 문제 삼지는 않겠습니다.

EQ 제작자나 음장에 신경쓰는 유저들은 이것저것 만져보고 바로 눈치 챌 듯 싶지만, 잘 모르는 일반 유저들에게 있어서는 이러한 오타는 빌립이 꼭 신경써줘야 하는 부분 아닌가, 하는 생각이 듭니다.

SRS WOW HD를 감상해 본 결과는 한마디로 말하자면 "훌륭했습니다."

특히 저음성향을 좋아하는 까닭에 반드시 저음성향의 이어폰과 음장을 고집하는 저에게 있어서 SRS WOW HD의 TruBass 기능은 정말 마음에 들었습니다. 과연 SRS WOW HD의 주요 기술이구나, 하는 생각이 딱 들었습니다.

사실 SRS 음장은 제가 5년 전에 아이리버 MP3 플레이어인 T10을 사용할 때와 2년전에 아이리버 W7 을 사용할 때 이미 충분히 겪어본 음장이었고, 그때 당시도 좋아했었는데, 이번 기회로 다시한번 좋은 음장 시스템이라는 것은 재확인 할 수 있었습니다.

그동안 사용해왔던 Cowon의 BBE+ 음장시스템과 비교해 보았을때, BBE+는 훨씬 전문가적이고 복잡한 느낌이 있는 반면에 (하나의 EQ를 만들기 위해 설정해야 하는 항목이 10가지 이상이나 됩니다.), SRS WOW HD는 5가지의 간단한 설정으로 웅장하고 화려한 사운드를 즐길 수 있다는 점이 좋았습니다.

##4. 정리

![][image12]

PMP는 포터블 "멀티미디어" 플레이어 라지만, 사실 PMP를 음악 감상을 위해서만 사는 이는 적습니다. 주로 영화 감상이나 인터넷 동영상 강의를 위해 구매하는 경우가 대부분이기 때문에 음악 감상은 부수적인 요소에 그치는 경우가 많습니다.

하지만 Viliv HD5 Clear의 경우는 음악 플레이어로서의 기능에 상당히 충실함을 알 수 있었습니다. SRS WOW HD의 채용은, HD5가 동영상 감상 기기로서 뿐만 아니라 훌륭한 음악 감상 기기로 탈바꿈 하도록 해 줍니다.

몇가지 수정할 점이 보이긴 하지만 - 특히 SRS 설정 부분에서 High와 스피커는 정말 고쳐져야 한다고 생각합니다.(High -> Definition / 스피커 -> WOW) SRS WOW HD의 채용에 대해서 상당히 크게 광고되고 있는 부분인데, 정작 설정창에서 그런 실수를 범할 줄은 몰랐습니다. - 지금 이대로도 상당한 음악 플레이어로서의 역할도 충분히 해 낼 수 있을 것 같습니다.

이상으로 2부를 마칩니다.

---

##하드코어 유저를 위한 추가 리뷰

HD5 Clear의 경우, 갭리스(Gapless) 재생 기능이 없습니다.

한 곡에서 다음곡으로 넘어가는 동안에 약 0.5초 정도의 공백이 생깁니다.

일반적인 유저들은 갭리스 재생이 불필요한 기능일 수도 있으나, 갭리스 재생이 필요한 이들에게는 좀 좋지 않은 소식이 되겠습니다. (저 개인으로서도 갭리스 재생을 필요로 하는 유저라서 조금 유감입니다.)

두번째로, 제품을 켜고 난 즉시 음악 플레이어로 들어가서 파일 탐색기를 통해 음악 선곡을 할때, 시스템이 전체적으로 약간 버벅거리는 현상이 있습니다. 이는 조금 기다리면 다시 원래대로의 반응속도를 가지게 되더군요. 큰 문제는 없을것으로 보입니다.

세번째로, 멀티테스킹은 제대로 정상작동합니다. (음악을 켠 채로 텍스트 뷰어를 보는 등.) 반응속도의 저하라던가는 특별히 느껴지지 않았습니다.

마음에 드셨다면 추천 부탁드립니다.

[image1]: http://i.imgur.com/A6QOiHE.jpg
[image2]: http://i.imgur.com/qKiDC5k.jpg
[image3]: http://i.imgur.com/KsHmzeM.jpg
[image4]: http://i.imgur.com/ccZdI8u.jpg
[image5]: http://i.imgur.com/RWmoNT2.png
[image6]: http://i.imgur.com/TUFSEpc.jpg
[image7]: http://i.imgur.com/pGPqd3Q.jpg
[image8]: http://i.imgur.com/TLkVH0l.jpg
[image9]: http://i.imgur.com/rgHJeTM.jpg
[image10]: http://i.imgur.com/ZsNmCBc.jpg
[image11]: http://i.imgur.com/36K34pB.jpg
[image12]: http://i.imgur.com/DQjX1hc.jpg