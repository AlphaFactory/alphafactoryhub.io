---
layout: post
title: Apple EarPods
date: 2012-12-11 13:45
image: http://i.imgur.com/PDmI4XN.jpg
thumbnail: http://i.imgur.com/PDmI4XNb.jpg
---
이어팟은 무난한 오픈형 이어폰인듯하다. 소리가 심심한 감이 없지 않아 있는데 이건 현재 내 주력 이어폰이 Sony XBA-3라서 어쩔수 없는듯. 정말 오랜만에 오픈형 이어폰을 들어보는데, 그냥저냥 들을만한 것 같다.
