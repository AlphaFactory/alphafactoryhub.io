---
layout: post
title: "XCOM : Enemy Unknown Demo"
date: 2012-10-09 22:42
thumbnail: http://i.imgur.com/QSudk6bb.jpg
---
플레이한지 꽤 됐는데, Steam에서 예약구매 봉인이 풀리는게 아직 3일이나 남아서 기다릴겸 다시 한번 되새겨 보는 마음으로 리뷰글을 올린다.

3일 후, 그러니까 2012년 10월 12일부터 Steam에서 플레이 가능하게 되는 XCOM : Enemy Unknown은 90년대 초반에 나온 동명의 게임의 리메이크작으로, 문명 시리즈로 유명한 시드마이어가 디렉터로 참여했다. 문명과 동일하게 Firaxis가 만들고 2K Games가 배급.

솔직히 개인적인 감상을 말하자면, HOMM과 스타크래프트를 적절히 섞으면 이런 게임이 나올거 같다. 진짜 재밌다. 기다리기가 너무 힘들다.

![][image1]

![][image2]

![][image3]

일단 한글화는 완벽하다. 데모라서 그런지 가끔 오타가 보이긴 하는데 대수롭진 않은 정도. 이번작 리드 디자이너가 한국시장 꽤 신경쓴다고 인터뷰에서 그랬었는데 사실인가보다.

> 우선 무엇보다 엑스콤을 한국 게이머들과 함께 나눌 수 있다는 점이 너무 좋다. 한국의 전략 게임이나 한국의 게이머들이 좋아하는 유형과 취향을 통해서 우리 역시 성장할 수 있는 좋은 기회라고 생각한다. 그렇기 때문에 이번 **새로운 엑스콤은 한국어로 번역**이 완료되어 있다. 한국 팬들이 엑스콤을 플레이해주는 것을 영광이라고 생각한다.
> <footer>인벤 기사 : <cite><a href="http://www.inven.co.kr/webzine/news/?news=46987">한글버전 준비완료! 엑스콤의 리드 디자이너 '제이크 솔로몬'</a></cite></footer>

데모까지 한글화는 솔직히 예상외였는데, 아마 M&M Heroes 6를 내가 구매했을 당시에, 본편은 한글화가 완벽했지만 데모는 영어판이었던 기억 때문이지않나 싶다.

![][image4]

이곳은 기지다. 굉장히 특이한 인터페이스라고 생각했는데 원작 XCOM 영상을 좀 본고 나서야 왜 이렇게 했는지 알것 같았다. 원작에서도 이런식으로 "옆면을 보는 듯한" 구조를 보여줬었기 때문인듯. 나름의 원작 살리기겠지.

![][image5]

![][image6]

외계인 갈아넣는 연구실이다. 우리가 가끔 농담조로 전자계통 기업들이 공돌이 갈아넣는다고 표현하지만 이건 그것과는 달리 외계인을 진짜로 갈아 넣는다. 이놈들이 현실에 있다면 그건 삼성과 인텔이겠지...외계인 고문하는 소리좀 안나게 하라! 무기/기술 개발에 외계인 무기 파편은 물론이고 외계인 시체가 들어간다.

![][image7]

![][image8]

![][image9]

비행기가 이륙하는 컷신과 함께 외계인이 나타났다는 장소로 날아간다. 비행기가 지구를 빙빙 도는 이 연출 역시도 XCOM의 원작에 나왔던 요소.

![][image10]

![][image11]

임무 시작. 일단 그래픽 옵션은 최상옵은 아니고 상옵정도다.

![][image12]

처음만난 외계인은 이렇게 클로즈업 씬과 함께 정보를 알려준다.

![][image13]

원작을 아직 제대로 해보진 못했지만 영상으로 비교해 본 결과, 상당히 원작을 잘 살렸다는 느낌이다. 팬들에 의하면 난이도가 원작보다 쉬워져서 그로 인해 특유의 공포스러운 분위기는 제대로 살리지 못한 것 같지만, 그건 어쩔 수 없는 부분일지도.

게임 진행은 HOMM에서 전투 부분을 생각하면 되겠다. 전통대로 턴제로 진행되고, 유닛 한명이 두번의 행동을 취할 수 있으며, 장거리 이동(돌격)시에는 1번만 행동을 취할 수 있다.

![][image14]

공격 명령에 들어가면 이렇게 TPS처럼 화면이 바뀌지만, 마우스로 실제 조준해서 쏘는 건 아니다. 어지간한 턴제 전략이 그렇듯, 확률에 의해 데미지를 주고 받기 때문에 완전히 엄폐하고 있어도 가끔 맞는 경우가 있다. 지금 조준하고 있는 외계인은 공격 한번에 골로 갔다.

![][image15]

적당히 미션 클리어. 게임이 매우 마음에 들었기 때문인지, 상당히 짧게 느껴졌다. (사실 스테이지 두개 클리어하면 데모가 바로 종료되는지라 실제로도 짧은건 맞지만)

![][image16]

![][image17]

![][image18]

기지로 돌아올때도 출발할 때와 마찬가지로 지구를 돌아와서 착륙하는 연출과 영상이 나온다. 동영상으로 된 부분은 없고 전부 실시간 인게임 렌더링.

![][image19]

![][image20]

![][image21]

![][image22]

![][image23]

기지로 돌아오면 참모의 심각한 얼굴과 함께, 알수 없는 적을 조우한 분대의 컷신이 나오면서 데모는 여기서 끝난다.

감상은 앞에서 적었으니 따로 정리하진 않겠다.

올해 연말은 이거만 믿고 가자. 족히 한두달은 빠져들어 재밌게 플레이 할 수 있을 듯하다. 지금 예약구매 하면 각종 게임 내 아이템을 특전으로 주는 것은 물론, 문명5를 공짜로 준다. 만약 본인이 문명 5를 가지고 있다면 친구한테 줄 수도 있고. 일단 데모만으로 따져봤을때는 굉장히 재미있었으니 3일만 더 참아보자.

PS) 스샷은 따로 안찍었는데 병영 가보면 병사 국적 중에 한국이 있다. ㅠ_ㅠb

[citeurl]: http://www.inven.co.kr/webzine/news/?news=46987

[image1]: http://i.imgur.com/L25Ea3A.jpg
[image2]: http://i.imgur.com/uoTvbKx.jpg
[image3]: http://i.imgur.com/cQlawEM.jpg
[image4]: http://i.imgur.com/YG0Gphh.jpg
[image5]: http://i.imgur.com/pyOZCR6.jpg
[image6]: http://i.imgur.com/O3dJlMZ.jpg
[image7]: http://i.imgur.com/2VjHwMb.jpg
[image8]: http://i.imgur.com/6HXFuyV.jpg
[image9]: http://i.imgur.com/bIMIN8P.jpg
[image10]: http://i.imgur.com/AICK09i.jpg
[image11]: http://i.imgur.com/nAoH6vl.jpg
[image12]: http://i.imgur.com/WiBwntB.jpg
[image13]: http://i.imgur.com/021wDR0.jpg
[image14]: http://i.imgur.com/m6VkhY1.jpg
[image15]: http://i.imgur.com/9ZD68P0.jpg
[image16]: http://i.imgur.com/XQnY3BX.jpg
[image17]: http://i.imgur.com/zWbh2Z7.jpg
[image18]: http://i.imgur.com/Uoy2yDk.jpg
[image19]: http://i.imgur.com/nlEpZtU.jpg
[image20]: http://i.imgur.com/bem6g07.jpg
[image21]: http://i.imgur.com/bItuneb.jpg
[image22]: http://i.imgur.com/FSbR4Kr.jpg
[image23]: http://i.imgur.com/QSudk6b.jpg