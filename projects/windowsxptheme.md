---
layout: page
title: Windows XP Theme
image: http://i.imgur.com/hco2Yf6.jpg
link: http://apps.alphafactory.co.kr/Windows_XP.zip
linktext: Download
permalink: /windowsxptheme/
---

그 어떤 모바일 기기용 Windows 테마 UCI들보다 가장 진짜 Windows에 가깝게 만들었다고 자부하는 UCI입니다. Cowon S9전용으로 만들어졌으며 J3와의 호환 여부는 기기가 없는 관계로 모르겠습니다. 안됬던 것으로 기억하고 있긴 합니다만. (말로는 J3 취급 안한다고 해왔지만 사실 조금 신경이 쓰이는 건 사실입니다.)

설치법이 복잡합니다!

설치법은 다른 메인 UCI와 동일하지만, 본 UCI의 가장 큰 특징인 “음악 UCI 셀렉터” 기능 때문에 본 UCI를 설치하게 되면 음악 UCI의 설치법이 좀 복잡해집니다. 루트 폴더에 Program Files라는 폴더를 만들고 그 아래에 Music 폴더를 생성합니다. 그리고 그 아래에 음악 UCI의 이름을 가진 폴더를 생성하고 그 폴더 안에 music.swf 파일과 프리뷰로 쓰일 cover.jpg 파일을 집어 넣습니다.

예를 들어, 큐브 뮤직과 루나님의 피아노를 동시에 쓰고 싶다면, Program Files/Music/CUBE Music/music.swf, Program Files/Music/Piano/music.swf 이런 식으로 여러개의 UCI 폴더를 생성하셔야 합니다.

{% include s9-uci-license.html %}

{% include no-more-support.html %}