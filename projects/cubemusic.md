---
layout: page
title: Cube Music
image: http://i.imgur.com/tSlYci2.jpg
link: http://apps.alphafactory.co.kr/music.zip
linktext: Download
permalink: /cubemusic/
---

설치법 및 사용법은 기존의 음악 UCI들과 같습니다. DAnNY1st님에 의해 디자인되었습니다.
초기버전만 만들어놓고 거의 손을 안댄 녀석이라 기능상으로 부족한 점이 많긴 합니다만 음악 재생에 별 무리는 없을 것이라 생각됩니다. 3D를 이용하여 EQ 설정부를 만들었었으나 S9이 결코 고사양이라고 보기는 힘들어서 영 부드럽진 않았던 걸로 기억합니다.

{% include s9-uci-license.html %}

{% include no-more-support.html %}