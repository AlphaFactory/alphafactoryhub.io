---
layout: page
title: Alpha's S9 Comics Viewer
image: http://i.imgur.com/r2ckKSa.jpg
link: http://apps.alphafactory.co.kr/comics.zip
linktext: Download
permalink: /comicsviewer/
---

설치법 및 사용법은 코원 미니 피엠피 카페에 제가 [2010년 1월 19일자로 작성했던 게시글](http://cafe.naver.com/d2d2d2/1508683)을 참고해 주세요. 해당 게시글은 최신인 Ver 2.10보다 한 단계 이전 버전인 Ver 2.05 기준으로 작성된 매뉴얼이지만, Ver 2.10에서 업데이트 된 것은 코믹뷰어 자체 음악 브라우저 뿐이므로 본 UCI를 사용하는 대체적인 방법에 있어서는 아무 문제가 없을것이라 생각됩니다.

압축파일을 열었을 때 comics.swf와 comics_browser.swf 파일이 있어야 정상입니다.

{% include s9-uci-license.html %}

{% include no-more-support.html %}