---
layout: page
title: Ez2Dj™ Manager
image: http://i.imgur.com/ZEUABHI.jpg
link: https://play.google.com/store/apps/details?id=com.alphafactory.ez2djmanager
linktext: Google Play Store
permalink: /ez2djmanager/
---

**게임 앱이 아닙니다.**

온라인 연동 지원이 되지 않는 아케이드 게임 Ez2Dj 시리즈를 위한 개인 점수 관리 어플리케이션입니다. 또한 전 시리즈의 모든 곡의 메타데이터, 레벨, 난이도 별 정보를 포함하고 있어 Ez2Dj 시리즈 곡의 정보 뷰어로도 사용하실 수 있습니다.

본 어플리케이션에 사용된 Ez2Dj 수록곡 정보는 Theme of Ez2Dj를 비롯한 각종 리듬게임 커뮤니티에서 활동하는 분들이 정리하신 자료들과 각종 위키를 참고한 것으로써, 최신 정보가 반영되어 있지 않을 수 있으므로 유저분들의 적극적인 피드백을 부탁드립니다.

본 어플리케이션에 사용된 Ez2Dj 수록곡의 난이도별 타이틀 이미지의 저작권은 어뮤즈월드(AMUSE WORLD) 및 각 타이틀 이미지 제작자에게 있습니다.

{% include no-more-support.html %}

---

###Ver 1.1.4

- 일부 곡 레벨 정보 수정
- 곡별 타이틀 이미지 100개 이상 추가
- 메모리 누수 방지 알고리즘 추가

###Ver 1.1.3

- 7Street 레벨 정보 대량 업데이트
- DB 업데이트 속도 향상

###Ver 1.1.2

- 타이틀 이미지 일부 추가

###Ver 1.1.1

- 곡 레벨 정보 일부 수정

###Ver 1.1.0

- 7th Trax 이후에 잘못되어 있던 DB들 대량 수정
- 수록곡별 타이틀 이미지 100여개 추가
- 화면을 가로로 회전해도 제대로 표시되도록 수정
- 인증사진 크게 보기 기능 추가
- 기록 삭제시 인증사진 삭제되지 않던 문제 수정

###Ver 1.0.0

- Ez2Dj™ Manager 릴리즈